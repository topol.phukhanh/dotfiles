# The Q dotfiles

![screenshot](./images/screenshot.png)

## Steps
```bash
# install path must be at `$HOME/dotfiles`
git clone git@gitlab.com:topol.phukhanh/dotfiles.git ~/
cd ~/dotfiles
```

### 1. Install platform base first
**In each platform**
```bash
brew bundle
```

- [Linux](./platform/linux)
- [MacOs](./platform/darwin)

### 2. Bootstrap dotfiles
```bash
bash ./install.sh
```


## Q Utils
*Install [Rust](https://www.rust-lang.org/tools/install)*

```
$ cargo build --release
$ cp ./target/release/yaml2json ~/.local/bin
```

## Misc
### Install latest nvim?
```
curl -fsSL https://github.com/neovim/neovim/releases/download/nightly/nvim-linux64.tar.gz | tar xz --strip-components=1 -C /opt/neovim
# or macos
curl -fsSL https://github.com/neovim/neovim/releases/download/nightly/nvim-macos.tar.gz | tar xz --strip-components=1 -C /opt/neovim
```

# License
[MIT License](LICENSE)


# Note
- brew install python@3.8
- echo 'export PATH="/usr/local/opt/python@3.8/bin:$PATH"' >> ~/.zshrc
- brew install python@3.9
- dotfiles/platform/darwin/basis.sh >> rust, source $HOME/.cargo/env
- dotfiles/scripts/dotfiles.sh
- dotfiles/scripts/install_latest_nvim.sh
- font book
- command 'vi' >> ':pluginstall', Details: https://github.com/junegunn/vim-plug

